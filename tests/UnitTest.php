<?php

namespace App\tests;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


use App\Form\EditUserType;
use App\Controller\RegistrationController;
use PHPUnit\Framework\TestCase;
use App\Entity\User;

final class UnitTest extends TestCase
{
    public function testFailure(): void
    {
        $machin = new User();
        $machin->setEmail("test@mail.com");

        $this->assertStringContainsString("@", $machin->getEmail());
    }
    public function testRole(): void
    {
        $truc = new User();
        $truc->setRoles(["MODO"]);

        $this->assertContains("ROLE_USER", $truc->getRoles());
    }
}
