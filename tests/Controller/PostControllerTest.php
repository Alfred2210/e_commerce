<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{
    public function testSomething(): void
    {
        // This calls KernelTestCase::bootKernel(), and creates a
        // "client" that is acting as the browser
        $User = static::createClient();

        // Request a specific page
        $testGet = $User->request('GET', '/user');

        // Validate a successful response and some content
        $this->assertSelectorTextContains('title', 'Redirecting to /user/login');
    }
}
