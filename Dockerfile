FROM alpine:3.17
RUN apk update && apk upgrade
RUN apk add wget
RUN apk add php php-cli php-phar php-json php-dom php-pdo php-mbstring curl unzip php-openssl php-mbstring 
RUN apk --no-cache add bash
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php composer-setup.php && php -r "unlink('composer-setup.php');" && mv composer.phar /usr/local/bin/composer
RUN apk add php-xml 
RUN apk add php-mbstring
RUN apk add php-iconv
RUN apk add php-xmlwriter
RUN apk add php-tokenizer
RUN apk add php-ctype
RUN apk add php-session
RUN apk add php-simplexml
RUN wget https://get.symfony.com/cli/installer -O - | bash && mv /root/.symfony5/bin/symfony /usr/local/bin/symfony
RUN apk add php-intl
RUN apk add php-pdo_mysql
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .
RUN cat /etc/php81/php.ini
RUN composer install
RUN symfony check:requirements
CMD symfony server:start